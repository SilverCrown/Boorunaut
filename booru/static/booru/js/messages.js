$(document).ready(function() {

    /*
        ===== DM HANDLING =====
    */

    // Handle user cancellation gracefully
    $('#cancelDm').click(function(e) {
        console.log(e);
        e.preventDefault();
        window.location.href='/inbox';
    });

    $('#createDm').click(function(e) {
        e.preventDefault();
        console.log(e);
        var send_data = $("#sendDm").serialize();
        console.log(send_data)

        // Scroll to top once bar is clicked
        $('html, body').animate({
            scrollTop: $("#serverMessage").offset().top
        }, 750);

        $.post({
            url: '/inbox/send',
            data: send_data,
            success: function(data) {
                console.log(data);
                // Clear out empty alerts container
                $("#serverMessage").empty("");
                if (status === 0) {
                    // Didn't work
                    $("#serverMessage").html("<div class='alert alert-danger'>" + data.message + "</div>");
                } else {
                    // Sent
                    $("#serverMessage").html("<div class='alert alert-success'>" + data.message + "</div>");
                }
            },
            error: function(data) {
                console.error(data);
            }
        });
    });
});