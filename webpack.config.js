/*
<https://webpack.js.org/guides/getting-started/>
*/

const path = require('path');

module.exports = {
  entry: './booru/static/booru/js/site.js',
  output: {
    filename: 'site.js',
    path: path.resolve(__dirname, 'dist'),
  },
};